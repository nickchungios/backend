'use strict';

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _socket = require('socket.io');

var _socket2 = _interopRequireDefault(_socket);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _tsData = require('./model/tsData');

var _tsData2 = _interopRequireDefault(_tsData);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

var _util = require('util');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LocalStrategy = require('passport-local').Strategy;


var app = (0, _express2.default)();
app.server = _http2.default.createServer(app);
var io = (0, _socket2.default)(app.server);

app.use("*", function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  if (req.method === 'OPTIONS') {
    res.send(200);
  } else {
    next();
  }
});

//middleware
//parse application/json
app.use(_bodyParser2.default.json({
  limit: _config2.default.bodyLimit
}));

//passport config
app.use(_passport2.default.initialize());
var Account = require('./model/account');
_passport2.default.use(new LocalStrategy({
  usernameField: 'email',
  passportField: 'passport'
}, Account.authenticate()));
_passport2.default.serializeUser(Account.serializeUser());
_passport2.default.deserializeUser(Account.deserializeUser());

//api routes v1
app.use('/api/v1', _routes2.default);

// Base URL test endpoint to see if API is running
app.get('/', function (req, res) {
  res.json({ message: 'API is ALIVE!' });
});

app.get('/mail/:email&:name', function (req, res) {
  if (res.status(200) || res.status(204)) {
    // conaole.log(getJwtToken());
    getJwtToken(sendMail, req.params.email, req.params.name);
    res.json({ message: 'Mail Sent!!' });
  } else {
    res.json({ err: 'fuck' });
  }
});
// reservation email
app.get('/ssip/:name&:email&:room&:start&:end', function (req, res) {
  if (res.status(200) || res.status(204)) {
    // conaole.log(getJwtToken());
    getJwtTokenSSIP(sendReservationMail, req.params.name, req.params.email, req.params.room, req.params.start, req.params.end);
    res.json({ message: 'Mail Sent!!' });
  } else {
    res.json({ err: 'fuck' });
  }
});

//get Token
var JWT = require('jwt-simple');
var httpRequest = require('request');
var btoa = require('btoa');

app.get('/token', function (req, res) {
  var JWTtoken = '';

  var clientId = "sagtwdev-vuetest-1.0.0" || 'localdev';
  var clientSecret = "kVyzMHvQsHZRfVCw2MtEcqvwX7rWh7HN9yPNeTeT7VS" || 'localdev';
  var appName = "vuetest" || 'localdev';
  var appVersion = "1.0.0" || 'localdev';

  var stringToEncode = clientId + ':' + clientSecret;
  var base64 = btoa(stringToEncode);

  console.log('stringToEncode:' + stringToEncode);
  console.log('clientId:' + clientId);
  console.log('clientSecret:' + clientSecret);
  console.log('base64:' + base64);

  var jsondata = {
    appName: appName,
    appVersion: appVersion,
    hostTenant: 'sagtwdev',
    userTenant: 'sagtwdev'
  };

  //   //fetch JWT
  httpRequest.post({
    url: 'https://gateway.eu1.mindsphere.io/api/technicaltokenmanager/v3/oauth/token',
    headers: { 'X-SPACE-AUTH-KEY': 'Basic ' + base64, 'content-type': 'application/json' },
    json: jsondata
  }, function (error, answer, body) {
    if (!error) {

      if (answer.statusCode == 200) {
        var response = body;
        console.log('Response: ' + body);

        if (response.access_token) {
          JWTtoken = response.access_token;
          console.log('AccessToken: ' + JWTtoken);
          res.json(JWTtoken);
          try {
            var decodedJWT = JWT.decode(response.access_token, null, true);
            console.log('decoded AccessToken: ' + JSON.stringify(decodedJWT));
          } catch (e) {
            console.log("error occured when decoding JWT", e);
          }
        }
      } else {
        console.log('Error: ' + error);
      }
    } else {
      console.log('Error: ' + error);
    }
  });
});

var JWTtoken = '';
// send mail function
function sendMail(token, email, name) {

  var body = {
    "body": {
      "testKey": name,
      "testValue": 'it is an email for demo case implementation'
    },
    "messageCategoryId": 3443,
    "recipientsTo": email,
    "from": 'AutomationShow@mindsphere.io',
    "subject": 'Automation Show',
    "priority": 3
  };

  httpRequest.post({
    url: 'https://gateway.eu1.mindsphere.io/api/notification/v3/publisher/messages',
    headers: { 'authorization': 'Bearer ' + token, 'content-type': 'application/json' },
    json: body
  }, function (error, answer, body) {
    if (!error) {

      if (answer.statusCode == 204) {
        console.log('Successfully Send Mail!!');
      } else {
        console.log('Error: ' + error);
      }
    } else {
      console.log('Error: ' + error);
    }
  });
}
// token function
function getJwtToken(callback, email, name) {

  var clientId = "sagtwdev-vuetest-1.0.0" || 'localdev';
  var clientSecret = "kVyzMHvQsHZRfVCw2MtEcqvwX7rWh7HN9yPNeTeT7VS" || 'localdev';
  var appName = "vuetest" || 'localdev';
  var appVersion = "1.0.0" || 'localdev';

  var stringToEncode = clientId + ':' + clientSecret;
  var base64 = btoa(stringToEncode);

  // console.log('stringToEncode:' + stringToEncode);
  // console.log('clientId:' + clientId);
  // console.log('clientSecret:' + clientSecret);
  // console.log('base64:' + base64);

  var jsondata = {
    appName: appName,
    appVersion: appVersion,
    hostTenant: 'sagtwdev',
    userTenant: 'sagtwdev'
  };

  //   //fetch JWT
  httpRequest.post({
    url: 'https://gateway.eu1.mindsphere.io/api/technicaltokenmanager/v3/oauth/token',
    headers: { 'X-SPACE-AUTH-KEY': 'Basic ' + base64, 'content-type': 'application/json' },
    json: jsondata
  }, function (error, answer, body) {
    if (!error) {

      if (answer.statusCode == 200) {
        var response = body;
        // console.log('Response: ' + body);

        if (response.access_token) {
          JWTtoken = response.access_token;
          // console.log('AccessToken: ' + JWTtoken);
          try {
            var decodedJWT = JWT.decode(response.access_token, null, true);
            // console.log('decoded AccessToken: ' + JSON.stringify(decodedJWT));
          } catch (e) {
            // console.log("error occured when decoding JWT", e);
          }
          callback(response.access_token, email, name);
        }
      } else {
        console.log('Error: ' + error);
      }
    } else {
      console.log('Error: ' + error);
    }
  });
}

// token function
function getJwtTokenSSIP(callback, name, email, room, start, end) {

  var clientId = "sagtwdev-vuetest-1.0.0" || 'localdev';
  var clientSecret = "kVyzMHvQsHZRfVCw2MtEcqvwX7rWh7HN9yPNeTeT7VS" || 'localdev';
  var appName = "vuetest" || 'localdev';
  var appVersion = "1.0.0" || 'localdev';

  var stringToEncode = clientId + ':' + clientSecret;
  var base64 = btoa(stringToEncode);

  // console.log('stringToEncode:' + stringToEncode);
  // console.log('clientId:' + clientId);
  // console.log('clientSecret:' + clientSecret);
  // console.log('base64:' + base64);

  var jsondata = {
    appName: appName,
    appVersion: appVersion,
    hostTenant: 'sagtwdev',
    userTenant: 'sagtwdev'
  };

  //   //fetch JWT
  httpRequest.post({
    url: 'https://gateway.eu1.mindsphere.io/api/technicaltokenmanager/v3/oauth/token',
    headers: { 'X-SPACE-AUTH-KEY': 'Basic ' + base64, 'content-type': 'application/json' },
    json: jsondata
  }, function (error, answer, body) {
    if (!error) {

      if (answer.statusCode == 200) {
        var response = body;
        // console.log('Response: ' + body);

        if (response.access_token) {
          JWTtoken = response.access_token;
          // console.log('AccessToken: ' + JWTtoken);
          try {
            var decodedJWT = JWT.decode(response.access_token, null, true);
            // console.log('decoded AccessToken: ' + JSON.stringify(decodedJWT));
          } catch (e) {
            // console.log("error occured when decoding JWT", e);
          }
          callback(response.access_token, name, email, room, start, end);
        }
      } else {
        console.log('Error: ' + error);
      }
    } else {
      console.log('Error: ' + error);
    }
  });
}

// send reservation mail function
function sendReservationMail(token, name, email, room, start, end) {

  var body = {
    "body": {
      "testKey": name,
      "testValue": 'Your ' + room + ' meeting room reservation between ' + start + ' to ' + end + ' has done!!'
    },
    "messageCategoryId": 3443,
    "recipientsTo": email,
    "from": 'SSIP@mindsphere.io',
    "subject": 'Siemens Summer Internship Program',
    "priority": 3
  };

  httpRequest.post({
    url: 'https://gateway.eu1.mindsphere.io/api/notification/v3/publisher/messages',
    headers: { 'authorization': 'Bearer ' + token, 'content-type': 'application/json' },
    json: body
  }, function (error, answer, body) {
    if (!error) {

      if (answer.statusCode == 204) {
        console.log('Successfully Send Mail!!');
      } else {
        console.log('Error: ' + error);
      }
    } else {
      console.log('Error: ' + error);
    }
  });
}

/*||||||||||||||||SOCKET|||||||||||||||||||||||*/
//Listen for connection
io.on('connection', function (client) {
  console.log('a user connected.');
  //Listens for a new data.
  client.on('newData', function (AbnormalOutput, AbnormalOutput_qc, CountingArrival, CountingArrival_qc, CurrentValueCounter, CurrentValueCounter_qc, R1axisLoad, R1axisLoad_qc, SetpointCounter, SetpointCounter_qc, XaxisLoad, XaxisLoad_qc, YaxisLoad, YaxisLoad_qc, ZaxisLoad, ZaxisLoad_qc) {
    //Create data
    var newData = new _tsData2.default({
      AbnormalOutput: AbnormalOutput,
      AbnormalOutput_qc: AbnormalOutput_qc,
      CountingArrival: CountingArrival,
      CountingArrival_qc: CountingArrival_qc,
      CurrentValueCounter: CurrentValueCounter,
      CurrentValueCounter_qc: CurrentValueCounter_qc,
      R1axisLoad: R1axisLoad,
      R1axisLoad_qc: R1axisLoad_qc,
      SetpointCounter: SetpointCounter,
      SetpointCounter_qc: SetpointCounter_qc,
      XaxisLoad: XaxisLoad,
      XaxisLoad_qc: XaxisLoad_qc,
      YaxisLoad: YaxisLoad,
      YaxisLoad_qc: YaxisLoad_qc,
      ZaxisLoad: ZaxisLoad,
      ZaxisLoad_qc: ZaxisLoad_qc
    });
    //Save it to database
    newData.save(function (err, tsData) {
      console.log('new data created.');
      io.emit("dataCreated", tsData.AbnormalOutput, tsData.AbnormalOutput_qc, tsData.CountingArrival, tsData.CurrentValueCounter, tsData.CurrentValueCounter_qc, tsData.R1axisLoad, tsData.R1axisLoad_qc, tsData.SetpointCounter, tsData.SetpointCounter_qc, tsData.XaxisLoad, tsData.XaxisLoad_qc, tsData.YaxisLoad, tsData.YaxisLoad_qc, tsData.ZaxisLoad, tsData.ZaxisLoad_qc, tsData._time, tsData.id);
    });
  });
});
/*||||||||||||||||||||END SOCKETS||||||||||||||||||*/

app.server.listen(_config2.default.port);
console.log('Started on port ' + app.server.address().port);

module.exports = {
  app: app,
  io: io
};
//# sourceMappingURL=index.js.map