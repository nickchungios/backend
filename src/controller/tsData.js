import mongoose from 'mongoose';
import { Router } from 'express';
import TsData from '../model/tsData';
import bodyParser from 'body-parser';

export default({ config, db }) => {
    let api = Router();
  
    // CRUD - Create Read Update Delete
  
    // '/v1/tsData/add' - Create
    api.post('/add', (req, res) => {
        let newData = new TsData();
         newData.AbnormalOutput = req.body.AbnormalOutput;
         newData.AbnormalOutput_qc = req.body.AbnormalOutput_qc;
         newData.CountingArrival = req.body.CountingArrival;
         newData.CountingArrival_qc = req.body.CountingArrival_qc;
         newData.CurrentValueCounter = req.body.CurrentValueCounter;
         newData.CurrentValueCounter_qc = req.body.CountingArrival_qc;
         newData.R1axisLoad = req.body.R1axisLoad;
         newData.R1axisLoad_qc = req.body.R1axisLoad_qc;
         newData.SetpointCounter = req.body.SetpointCounter;
         newData.SetpointCounter_qc = req.body.SetpointCounter_qc;
         newData.XaxisLoad = req.body.XaxisLoad;
         newData.XaxisLoad_qc = req.body.XaxisLoad_qc;
         newData.YaxisLoad = req.body.YaxisLoad;
         newData.YaxisLoad_qc = req.body.YaxisLoad_qc;
         newData.ZaxisLoad = req.body.ZaxisLoad;
         newData.ZaxisLoad_qc = req.body.ZaxisLoad_qc;

    newData.save(err => {
      if (err) {
        res.status(500).json({ message: err });
      }
        res.status(200).json({ message: 'New time series data saved successfully' });
    });
    });
  
    // '/v1/tsData/' - Read
    api.get('/', (req, res) => {
      TsData.find({}, (err, tsData) => {
        if (err) {
          res.status(500).send({message: err});
        }
        res.status(200).json(tsData);
      });
    });
  
    // '/v1/tsData/:id' - Read 1
    api.get('/:id', (req, res) => {
      TsData.findById(req.params.id, (err, tsData) => {
        if (err) {
            res.status(500).send({message: err});
          }
          res.status(200).json(tsData);
        });
    });

    // '/v1/tsData/:id' - PUT - update an existing record
    api.put('/:id', (req, res) => {
      TsData.findById(req.params.id, (err, tsData) => {
        if (err) {
            res.status(500).json({ message: err });
          }
         newData.AbnormalOutput = req.body.AbnormalOutput;
         newData.AbnormalOutput_qc = req.body.AbnormalOutput_qc;
         newData.CountingArrival = req.body.CountingArrival;
         newData.CountingArrival_qc = req.body.CountingArrival_qc;
         newData.CurrentValueCounter = req.body.CurrentValueCounter;
         newData.CurrentValueCounter_qc = req.body.CountingArrival_qc;
         newData.R1axisLoad = req.body.R1axisLoad;
         newData.R1axisLoad_qc = req.body.R1axisLoad_qc;
         newData.SetpointCounter = req.body.SetpointCounter;
         newData.SetpointCounter_qc = req.body.SetpointCounter_qc;
         newData.XaxisLoad = req.body.XaxisLoad;
         newData.XaxisLoad_qc = req.body.XaxisLoad_qc;
         newData.YaxisLoad = req.body.YaxisLoad;
         newData.YaxisLoad_qc = req.body.YaxisLoad_qc;
         newData.ZaxisLoad = req.body.ZaxisLoad;
         newData.ZaxisLoad_qc = req.body.ZaxisLoad_qc;

        tsData.save(function(err) {
          if (err) {
          res.status(500).json({ message: err });
          }
          res.status(200).json({ message: 'Time series data updated' });
        });
      });
    });
  
    // '/v1/tsData/:id' - DELETE - remove a time series data
    api.delete('/:id', (req, res) => {
        TsData.remove({
            _id: req.params.id
          }, (err, tsData) => {
            if (err) {
              res.status(500).json({ message: err });
            }
            res.status(200).json({ message: 'Time series data Successfully Removed'});
          });
    });
  
    return api;
  }
  