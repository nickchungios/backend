import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import socket from 'socket.io'
import passport from 'passport';
const LocalStrategy = require('passport-local').Strategy;
import tsData from './model/tsData';

import config from './config';
import routes from './routes';
import { isObject } from 'util';

let app = express();
app.server = http.createServer(app);
let io = socket(app.server);

app.use("*", function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
  if (req.method === 'OPTIONS') {
    res.send(200)
  } else {
    next()
  }
});

//middleware
//parse application/json
app.use(bodyParser.json({
  limit: config.bodyLimit
}));

//passport config
app.use(passport.initialize());
let Account = require('./model/account');
passport.use(new LocalStrategy({
  usernameField: 'email',
  passportField: 'passport'
},
  Account.authenticate()
));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

//api routes v1
app.use('/api/v1', routes);

// Base URL test endpoint to see if API is running
app.get('/', (req, res) => {
    res.json({ message: 'API is ALIVE!' })
  });

//   app.get('/mail/:email&:name', (req, res) => {
//     if(res.status(200) || res.status(204)){
//       // conaole.log(getJwtToken());
//       getJwtToken(sendMail,req.params.email,req.params.name);
//       res.json({message: 'Mail Sent!!'});
//     }else{
//       res.json({err: 'fuck'});
//     }
//   });
// // reservation email
//   app.get('/ssip/:name&:email&:room&:start&:end', (req, res) => {
//     if(res.status(200) || res.status(204)){
//       // conaole.log(getJwtToken());
//       getJwtTokenSSIP(sendReservationMail,req.params.name,req.params.email,req.params.room,req.params.start,req.params.end);
//       res.json({message: 'Mail Sent!!'});
//     }else{
//       res.json({err: 'fuck'});
//     }
//   });

// //get Token
//   var JWT = require('jwt-simple');
//   var httpRequest = require('request');
//   var btoa = require('btoa');

//   app.get('/token', (req, res) => {
//     var JWTtoken = '';
  
//   var clientId = "sagtwdev-vuetest-1.0.0" || 'localdev'
//   var clientSecret = "kVyzMHvQsHZRfVCw2MtEcqvwX7rWh7HN9yPNeTeT7VS" || 'localdev';
//   var appName = "vuetest" || 'localdev';
//   var appVersion = "1.0.0" || 'localdev';

//   var stringToEncode = clientId + ':' + clientSecret;
//   var base64 = btoa(stringToEncode);

//   console.log('stringToEncode:' + stringToEncode);
//   console.log('clientId:' + clientId);
//   console.log('clientSecret:' + clientSecret);
//   console.log('base64:' + base64);

//   var jsondata = {
//       appName: appName,
//       appVersion: appVersion,
//       hostTenant: 'sagtwdev',
//       userTenant: 'sagtwdev'
//   };

//   //   //fetch JWT
//   httpRequest.post({
//       url: 'https://gateway.eu1.mindsphere.io/api/technicaltokenmanager/v3/oauth/token',
//       headers: { 'X-SPACE-AUTH-KEY': 'Basic ' + base64, 'content-type': 'application/json' },
//       json: jsondata
//   }, function (error, answer, body) {
//       if (!error) {

//       if (answer.statusCode == 200) {
//           var response = body;
//           console.log('Response: ' + body);

//           if (response.access_token) {
//             JWTtoken = response.access_token;
//           console.log('AccessToken: ' + JWTtoken);
//               res.json(JWTtoken)
//           try {
//               var decodedJWT = JWT.decode(response.access_token, null, true);
//               console.log('decoded AccessToken: ' + JSON.stringify(decodedJWT));
//           } catch (e) {
//               console.log("error occured when decoding JWT", e);
//           }
//           }
//       }
//       else {
//           console.log('Error: ' + error);
//       }
//       }
//       else {
//       console.log('Error: ' + error);
//       }
//   });
//   });

//   var JWTtoken = '';
//   // send mail function
//   function  sendMail(token,email,name) {

//     var body = {
//       "body": {
//       "testKey": name,
//       "testValue":'it is an email for demo case implementation'
//         },
//       "messageCategoryId":3443,
//       "recipientsTo": email,
//       "from":'AutomationShow@mindsphere.io',
//       "subject":'Automation Show',
//       "priority":3
//     }

//     httpRequest.post({
//       url: 'https://gateway.eu1.mindsphere.io/api/notification/v3/publisher/messages',
//       headers: { 'authorization': 'Bearer ' + token, 'content-type': 'application/json' },
//       json: body
//   }, function (error, answer, body) {
//       if (!error) {

//       if (answer.statusCode == 204) {
//           console.log('Successfully Send Mail!!');
//       }
//       else {
//           console.log('Error: ' + error);
//       }
//       }
//       else {
//       console.log('Error: ' + error);
//       }
//   });
//   }
//   // token function
//   function getJwtToken(callback,email,name) {
  
  
//   var clientId = "sagtwdev-vuetest-1.0.0" || 'localdev'
//   var clientSecret = "kVyzMHvQsHZRfVCw2MtEcqvwX7rWh7HN9yPNeTeT7VS" || 'localdev';
//   var appName = "vuetest" || 'localdev';
//   var appVersion = "1.0.0" || 'localdev';

//   var stringToEncode = clientId + ':' + clientSecret;
//   var base64 = btoa(stringToEncode);

//   // console.log('stringToEncode:' + stringToEncode);
//   // console.log('clientId:' + clientId);
//   // console.log('clientSecret:' + clientSecret);
//   // console.log('base64:' + base64);

//   var jsondata = {
//       appName: appName,
//       appVersion: appVersion,
//       hostTenant: 'sagtwdev',
//       userTenant: 'sagtwdev'
//   };

//   //   //fetch JWT
//   httpRequest.post({
//       url: 'https://gateway.eu1.mindsphere.io/api/technicaltokenmanager/v3/oauth/token',
//       headers: { 'X-SPACE-AUTH-KEY': 'Basic ' + base64, 'content-type': 'application/json' },
//       json: jsondata
//   }, function (error, answer, body) {
//       if (!error) {

//       if (answer.statusCode == 200) {
//           var response = body;
//           // console.log('Response: ' + body);

//           if (response.access_token) {
//             JWTtoken = response.access_token;
//           // console.log('AccessToken: ' + JWTtoken);
//           try {
//               var decodedJWT = JWT.decode(response.access_token, null, true);
//               // console.log('decoded AccessToken: ' + JSON.stringify(decodedJWT));
//           } catch (e) {
//               // console.log("error occured when decoding JWT", e);
//           }
//           callback(response.access_token,email,name);
//           }
//       }
//       else {
//           console.log('Error: ' + error);
//       }
//       }
//       else {
//       console.log('Error: ' + error);
//       }
//   });
//   }
  
//    // token function
//    function getJwtTokenSSIP(callback,name,email,room,start,end) {
  
  
//     var clientId = "sagtwdev-vuetest-1.0.0" || 'localdev'
//     var clientSecret = "kVyzMHvQsHZRfVCw2MtEcqvwX7rWh7HN9yPNeTeT7VS" || 'localdev';
//     var appName = "vuetest" || 'localdev';
//     var appVersion = "1.0.0" || 'localdev';
  
//     var stringToEncode = clientId + ':' + clientSecret;
//     var base64 = btoa(stringToEncode);
  
//     // console.log('stringToEncode:' + stringToEncode);
//     // console.log('clientId:' + clientId);
//     // console.log('clientSecret:' + clientSecret);
//     // console.log('base64:' + base64);
  
//     var jsondata = {
//         appName: appName,
//         appVersion: appVersion,
//         hostTenant: 'sagtwdev',
//         userTenant: 'sagtwdev'
//     };
  
//     //   //fetch JWT
//     httpRequest.post({
//         url: 'https://gateway.eu1.mindsphere.io/api/technicaltokenmanager/v3/oauth/token',
//         headers: { 'X-SPACE-AUTH-KEY': 'Basic ' + base64, 'content-type': 'application/json' },
//         json: jsondata
//     }, function (error, answer, body) {
//         if (!error) {
  
//         if (answer.statusCode == 200) {
//             var response = body;
//             // console.log('Response: ' + body);
  
//             if (response.access_token) {
//               JWTtoken = response.access_token;
//             // console.log('AccessToken: ' + JWTtoken);
//             try {
//                 var decodedJWT = JWT.decode(response.access_token, null, true);
//                 // console.log('decoded AccessToken: ' + JSON.stringify(decodedJWT));
//             } catch (e) {
//                 // console.log("error occured when decoding JWT", e);
//             }
//             callback(response.access_token,name,email,room,start,end);
//             }
//         }
//         else {
//             console.log('Error: ' + error);
//         }
//         }
//         else {
//         console.log('Error: ' + error);
//         }
//     });
//   }
  
//   // send reservation mail function
//   function  sendReservationMail(token,name,email,room,start,end) {

//     var body = {
//       "body": {
//       "testKey": name,
//       "testValue": 'Your ' + room + ' meeting room reservation between ' + start + ' to ' + end + ' has done!!'
//         },
//       "messageCategoryId":3443,
//       "recipientsTo": email,
//       "from":'SSIP@mindsphere.io',
//       "subject":'Siemens Summer Internship Program',
//       "priority":3
//     }

//     httpRequest.post({
//       url: 'https://gateway.eu1.mindsphere.io/api/notification/v3/publisher/messages',
//       headers: { 'authorization': 'Bearer ' + token, 'content-type': 'application/json' },
//       json: body
//   }, function (error, answer, body) {
//       if (!error) {

//       if (answer.statusCode == 204) {
//           console.log('Successfully Send Mail!!');
//       }
//       else {
//           console.log('Error: ' + error);
//       }
//       }
//       else {
//       console.log('Error: ' + error);
//       }
//   });
//   }


/*||||||||||||||||SOCKET|||||||||||||||||||||||*/
//Listen for connection
io.on('connection', function(client) {
  console.log('a user connected.');
  //Listens for a new data.
  client.on('newData', function(AbnormalOutput, AbnormalOutput_qc, CountingArrival, CountingArrival_qc, CurrentValueCounter, CurrentValueCounter_qc, 
    R1axisLoad, R1axisLoad_qc, SetpointCounter, SetpointCounter_qc, XaxisLoad, XaxisLoad_qc, YaxisLoad, YaxisLoad_qc, ZaxisLoad, ZaxisLoad_qc) {
      //Create data
      let newData = new tsData({
          AbnormalOutput: AbnormalOutput,
          AbnormalOutput_qc: AbnormalOutput_qc,
          CountingArrival: CountingArrival,
          CountingArrival_qc: CountingArrival_qc,
          CurrentValueCounter: CurrentValueCounter,
          CurrentValueCounter_qc: CurrentValueCounter_qc,
          R1axisLoad: R1axisLoad,
          R1axisLoad_qc: R1axisLoad_qc,
          SetpointCounter: SetpointCounter,
          SetpointCounter_qc: SetpointCounter_qc,
          XaxisLoad: XaxisLoad,
          XaxisLoad_qc: XaxisLoad_qc,
          YaxisLoad: YaxisLoad,
          YaxisLoad_qc: YaxisLoad_qc,
          ZaxisLoad: ZaxisLoad,
          ZaxisLoad_qc: ZaxisLoad_qc
      });
      //Save it to database
      newData.save(function(err, tsData) {
        console.log('new data created.');
        io.emit("dataCreated", tsData.AbnormalOutput, tsData.AbnormalOutput_qc, tsData.CountingArrival, tsData.CurrentValueCounter, tsData.CurrentValueCounter_qc,
         tsData.R1axisLoad, tsData.R1axisLoad_qc, tsData.SetpointCounter, tsData.SetpointCounter_qc, tsData.XaxisLoad, tsData.XaxisLoad_qc, tsData.YaxisLoad, tsData.YaxisLoad_qc,
         tsData.ZaxisLoad, tsData.ZaxisLoad_qc, tsData._time, tsData.id);
      });
    });
});
/*||||||||||||||||||||END SOCKETS||||||||||||||||||*/

app.server.listen(config.port);
console.log(`Started on port ${app.server.address().port}`);

module.exports = {
  app,
  io
}
