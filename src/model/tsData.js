import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const TsDataSchema = new Schema({
  AbnormalOutput: Boolean, default: "",
  AbnormalOutput_qc: Number, default: "",
  CountingArrival: Boolean, default: "",
  CountingArrival_qc: Number, default: "",
  CurrentValueCounter: Number, default: "",
  CurrentValueCounter_qc: Number, default: "",
  R1axisLoad:  Number, default: "",
  R1axisLoad_qc:  Number, default: "",
  SetpointCounter:  Number, default: "",
  SetpointCounter_qc:  Number, default: "",
  XaxisLoad:  Number, default: "",
  XaxisLoad_qc:  Number, default: "",
  YaxisLoad:  Number, default: "",
  YaxisLoad_qc:  Number, default: "",
  ZaxisLoad:  Number, default: "",
  ZaxisLoad_qc:  Number, default: "",
  _time: {type: Date, default: Date.now}
});



module.exports = mongoose.model('TsData', TsDataSchema);
